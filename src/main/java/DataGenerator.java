import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataGenerator {
    public static String replaceVarsIfPresent(String value) {
        if(value == null | value == "") { return value; }
        StringBuilder retText = new StringBuilder(value);

        final String startPat = "$!{";
        final String endPat = "}";

        while (retText.toString().contains(startPat)) {
            int st = retText.indexOf(startPat);
            int ed = retText.indexOf(endPat, st);
            String sub = retText.substring(st+startPat.length(), ed);

            if (sub.toLowerCase().contains("дата")) {
                System.out.println(sub);
                sub = replaceSubstringToDate(sub);
                retText.replace(st, ed+1, sub);
            } else {
                break;
            }
        }
        return retText.toString();
    }

    private static String replaceSubstringToDate(String string) {
        string = string.toLowerCase();
        string = string.replace(" ", "");

        LocalDate localDate = LocalDate.now();
        DateTimeFormatter formatDT = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        Pattern yearPat = Pattern.compile("[+-]+[\\d].*[\\s]*(год|лет)");
        Matcher yearMath = yearPat.matcher(string);
        Pattern monthPat = Pattern.compile("[+-]+[\\d]*[\\s]*мес.*[цав]*\\b");
        Matcher monthMath = monthPat.matcher(string);
        Pattern dayPat = Pattern.compile("[+-]+[\\s]*[\\d]*[\\s]*д[еняьй]*\\b");
        Matcher dayMath = dayPat.matcher(string);

        Pattern digitPat = Pattern.compile("[+-]+[\\s]*[\\d]*");

        if (yearMath.find()) {
            Matcher digitMath = digitPat.matcher(yearMath.group(0));
            digitMath.find();
            int yy = Integer.parseInt(digitMath.group(0));

            localDate = localDate.plusYears(yy);
        }
        if (monthMath.find()) {
            Matcher digitMath = digitPat.matcher(monthMath.group(0));
            digitMath.find();
            int mm = Integer.parseInt(digitMath.group(0));

            localDate = localDate.plusMonths(mm);
        }
        if (dayMath.find()) {
            Matcher digitMath = digitPat.matcher(dayMath.group(0));
            digitMath.find();
            int dd = Integer.parseInt(digitMath.group(0));

            localDate = localDate.plusDays(dd);
        }
        return formatDT.format(localDate);
    }
}
