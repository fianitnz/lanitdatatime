public class TestDataGen {
    public static void main(String[] args) {
        System.out.println(DataGenerator.replaceVarsIfPresent(
                "01234567890$!{дата(сегодня)}890$!{дата(сегодня)}"));
        System.out.println(DataGenerator.replaceVarsIfPresent(
                "Очень важно сообщение, сегодня: $!{дата(-1 день)} будет очень важное сообщение"));
        System.out.println(DataGenerator.replaceVarsIfPresent(
                "$!{дата(-1 год - 1 день +3 месяца)}"));
        System.out.println(DataGenerator.replaceVarsIfPresent(
                "$!{дата(-5 лет +7дней +3 мес)}"));
    }
}
